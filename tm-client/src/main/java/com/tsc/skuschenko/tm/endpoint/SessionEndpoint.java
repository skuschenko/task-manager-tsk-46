package com.tsc.skuschenko.tm.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

/**
 * This class was generated by Apache CXF 3.2.7
 * 2021-03-18T23:29:13.842+03:00
 * Generated source version: 3.2.7
 */
@WebService(targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "SessionEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface SessionEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/SessionEndpoint/closeSessionRequest", output = "http://endpoint.tm.skuschenko.tsc.com/SessionEndpoint/closeSessionResponse")
    @RequestWrapper(localName = "closeSession", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CloseSession")
    @ResponseWrapper(localName = "closeSessionResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.CloseSessionResponse")
    public void closeSession(
            @WebParam(name = "session", targetNamespace = "")
                    com.tsc.skuschenko.tm.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tm.skuschenko.tsc.com/SessionEndpoint/openSessionRequest", output = "http://endpoint.tm.skuschenko.tsc.com/SessionEndpoint/openSessionResponse")
    @RequestWrapper(localName = "openSession", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.OpenSession")
    @ResponseWrapper(localName = "openSessionResponse", targetNamespace = "http://endpoint.tm.skuschenko.tsc.com/", className = "com.tsc.skuschenko.tm.endpoint.OpenSessionResponse")
    @WebResult(name = "return", targetNamespace = "")
    public com.tsc.skuschenko.tm.endpoint.Session openSession(
            @WebParam(name = "login", targetNamespace = "")
                    java.lang.String login,
            @WebParam(name = "password", targetNamespace = "")
                    java.lang.String password
    );
}
