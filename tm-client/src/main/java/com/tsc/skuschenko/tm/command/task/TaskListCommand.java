package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public final class TaskListCommand extends AbstractTaskCommand {

    @NotNull
    private static final String DESCRIPTION = "show all tasks";

    @NotNull
    private static final String NAME = "task-list";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("sort");
        @NotNull final String sort = TerminalUtil.nextLine();
        @Nullable List<Task> tasks = new ArrayList<>();
        if (sort.isEmpty()) {
            tasks = serviceLocator.getTaskEndpoint().findTaskAll(session);
        } else {
            tasks = serviceLocator.getTaskEndpoint()
                    .findTaskAllSort(session, sort);
        }
        int index = 1;
        for (@NotNull final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
