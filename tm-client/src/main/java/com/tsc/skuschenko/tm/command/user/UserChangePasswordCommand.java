package com.tsc.skuschenko.tm.command.user;

import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @NotNull
    private static final String DESCRIPTION = "change password of current user";

    @NotNull
    private static final String NAME = "change-user-password";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @Nullable final Session session = serviceLocator.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        @NotNull final String userId =
                session.getUserId();
        showParameterInfo("new password");
        @NotNull final String password = TerminalUtil.nextLine();
        serviceLocator.getUserEndpoint().setPassword(userId, password);
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
