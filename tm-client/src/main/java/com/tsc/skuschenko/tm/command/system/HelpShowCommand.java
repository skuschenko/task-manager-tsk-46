package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;

public final class HelpShowCommand extends AbstractCommand {

    @NotNull
    private static final String ARGUMENT = "-h";

    @NotNull
    private static final String DESCRIPTION = "help";

    @NotNull
    private static final String NAME = "help";

    @NotNull
    @Override
    public String arg() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final Collection<AbstractCommand> commands =
                serviceLocator.getCommandService().getCommands();
        commands.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(item -> {
                    String result = "";
                    if (Optional.ofNullable(item.name()).isPresent()) {
                        result += item.name();
                    }
                    if (Optional.ofNullable(item.arg()).isPresent()) {
                        result += " [" + item.arg() + "]";
                    }
                    if (Optional.ofNullable(item.description()).isPresent()) {
                        result += " - " + item.description();
                    }
                    System.out.println(result);
                });
    }

    @NotNull
    @Override
    public String name() {
        return NAME;
    }

}
