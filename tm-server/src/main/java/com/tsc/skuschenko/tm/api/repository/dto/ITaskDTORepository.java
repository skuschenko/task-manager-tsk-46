package com.tsc.skuschenko.tm.api.repository.dto;

import com.tsc.skuschenko.tm.dto.TaskDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ITaskDTORepository extends IAbstractDTORepository<TaskDTO> {

    void clear(@NotNull String userId);

    void clearAllTasks();

    @Nullable List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAllTaskByProjectId(
            @NotNull String userId, @NotNull String projectId
    );

    @Nullable List<TaskDTO> findAllWithUserId(@NotNull String userId);

    @Nullable
    TaskDTO findById(@NotNull String id);

    @Nullable
    TaskDTO findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    TaskDTO findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    TaskDTO findOneByName(@NotNull String userId, @NotNull String name);

    void removeOneById(@NotNull String userId, @NotNull String taskId);

}
