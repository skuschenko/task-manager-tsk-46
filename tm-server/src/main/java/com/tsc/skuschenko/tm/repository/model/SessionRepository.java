package com.tsc.skuschenko.tm.repository.model;

import com.tsc.skuschenko.tm.api.repository.model.ISessionRepository;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.persistence.EntityManager;
import java.util.List;

public final class SessionRepository extends AbstractRepository<Session>
        implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public @Nullable List<Session> findAll(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM SessionDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(query, Session.class)
                .setParameter("userId", userId).getResultList();
    }

    @Override
    public Session findSessionById(Session session) {
        return entityManager.find(Session.class, session.getId());
    }

    @Nullable
    @Override
    public Session getReference(@NotNull final String id) {
        return entityManager.getReference(Session.class, id);
    }

    @Override
    public void removeSessionById(Session session) {
        @NotNull final String query =
                "DELETE FROM SessionDTO e WHERE e.id = :id";
        entityManager.createQuery(query, UserDTO.class)
                .setParameter("id", session.getId())
                .executeUpdate();
    }

}
