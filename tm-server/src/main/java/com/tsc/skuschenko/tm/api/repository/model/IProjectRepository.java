package com.tsc.skuschenko.tm.api.repository.model;

import com.tsc.skuschenko.tm.model.Project;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface IProjectRepository
        extends IAbstractRepository<Project> {

    void clear(@NotNull String userId);

    void clearAllProjects();

    @Nullable List<Project> findAll();

    @Nullable List<Project> findAllWithUserId(@NotNull String userId);

    @Nullable Project findById(@NotNull String id);

    @Nullable
    Project findOneById(@NotNull String userId, @NotNull String id);

    @Nullable
    Project findOneByIndex(@NotNull String userId, @NotNull Integer index);

    @Nullable
    Project findOneByName(@NotNull String userId, @NotNull String name);

    @Nullable
    Project getReference(@NotNull String id);

    void removeOneById(@NotNull String userId, @NotNull String projectId);

}