package com.tsc.skuschenko.tm.api.repository.dto;

import com.tsc.skuschenko.tm.dto.SessionDTO;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.List;

public interface ISessionDTORepository
        extends IAbstractDTORepository<SessionDTO> {

    void add(@NotNull SessionDTO session);

    @Nullable List<SessionDTO> findAll(@NotNull String userId);

    SessionDTO findSessionById(SessionDTO session);

    void removeSessionById(SessionDTO session);

}
