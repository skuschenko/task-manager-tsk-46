package com.tsc.skuschenko.tm.dto;

import com.tsc.skuschenko.tm.api.entity.IWBS;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@NoArgsConstructor
@Cacheable
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
@Table(name = "tm_project")
public final class ProjectDTO extends AbstractBusinessEntityDTO implements IWBS {

}
