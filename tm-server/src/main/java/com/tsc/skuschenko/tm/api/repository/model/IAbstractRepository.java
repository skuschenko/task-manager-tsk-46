package com.tsc.skuschenko.tm.api.repository.model;

import com.tsc.skuschenko.tm.model.AbstractEntity;
import org.jetbrains.annotations.NotNull;

import javax.persistence.TypedQuery;

public interface IAbstractRepository<E extends AbstractEntity> {

    void add(E entity);

    E getEntity(@NotNull TypedQuery<E> query);

    void remove(E entity);

    void update(E entity);

}
