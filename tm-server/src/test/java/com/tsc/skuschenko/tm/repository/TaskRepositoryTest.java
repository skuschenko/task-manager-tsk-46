package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.dto.ITaskDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.enumerated.Status;
import com.tsc.skuschenko.tm.repository.dto.TaskDTORepository;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;

public class TaskRepositoryTest {

    @NotNull
    final IPropertyService propertyService =
            new PropertyService();
    @NotNull
    final IConnectionService connectionService =
            new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager =
            connectionService.getEntityManager();

    @Test
    public void testChangeStatusById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus("status1");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals("status1", taskFind.getStatus());
    }

    @Test
    public void testChangeStatusByName() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus("status1");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals("status1", taskFind.getStatus());
    }

    @Test
    public void testClear() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        taskRepository.clearAllTasks();
        entityManager.getTransaction().commit();
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNull(taskFind);
    }

    @Test
    public void testCompleteById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByIndex() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCompleteByName() {
        @NotNull final TaskDTO task = testTaskModel();
        task.setStatus(Status.COMPLETE.getDisplayName());
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getName());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testCreate() {
        @NotNull final TaskDTO task = testTaskModel();
        testRepository(task);
    }


    @Test
    public void testFindOneById() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByIndex() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        entityManager.close();
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testFindOneByName() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        entityManager.close();
        Assert.assertNotNull(taskFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        taskRepository.removeOneById(task.getUserId(), task.getId());
        entityManager.getTransaction().commit();
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNull(taskFind);
    }

    @NotNull
    private ITaskDTORepository testRepository(@NotNull final TaskDTO task) {
        @NotNull final ITaskDTORepository taskRepository =
                new TaskDTORepository(entityManager);
        Assert.assertTrue(taskRepository
                .findAllWithUserId(task.getUserId()).isEmpty());
        taskRepository.add(task);
        entityManager.getTransaction().commit();
        @Nullable final TaskDTO taskById =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNotNull(taskById);
        Assert.assertEquals(taskById.getId(), task.getId());
        return taskRepository;
    }

    @Test
    public void testStartById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByIndex() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.IN_PROGRESS.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.IN_PROGRESS.getDisplayName(), taskFind.getStatus()
        );
    }

    @Test
    public void testStartByName() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setStatus(Status.COMPLETE.getDisplayName());
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getStatus());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByName(
                        task.getUserId(), task.getName()
                );
        entityManager.close();
        Assert.assertNotNull(taskFind.getStatus());
        Assert.assertEquals(
                Status.COMPLETE.getDisplayName(), taskFind.getStatus()
        );
    }

    @NotNull
    private TaskDTO testTaskModel() {
        @Nullable final TaskDTO task = new TaskDTO();
        task.setUserId("72729b26-01dd-4314-8d8c-40fb8577c6b5");
        task.setName("name1");
        task.setDescription("des1");
        Assert.assertNotNull(task);
        Assert.assertNotNull(task.getUserId());
        Assert.assertNotNull(task.getName());
        Assert.assertEquals("name1", task.getName());
        return task;
    }

    @Test
    public void testUpdateOneById() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setName("name2");
        task.setDescription("des2");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final TaskDTO taskFind =
                taskRepository.findById(task.getId());
        entityManager.close();
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }


    @Test
    public void testUpdateOneByIndex() {
        @NotNull final TaskDTO task = testTaskModel();
        @NotNull final ITaskDTORepository
                taskRepository = testRepository(task);
        task.setName("name2");
        task.setDescription("des2");
        entityManager.getTransaction().begin();
        taskRepository.update(task);
        entityManager.getTransaction().commit();
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        @Nullable final TaskDTO taskFind =
                taskRepository.findOneByIndex(task.getUserId(), 0);
        entityManager.close();
        Assert.assertNotNull(task.getName());
        Assert.assertNotNull(task.getDescription());
        Assert.assertEquals("name2", taskFind.getName());
        Assert.assertEquals("des2", taskFind.getDescription());
    }

}
