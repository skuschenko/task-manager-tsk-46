package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.dto.IUserDTORepository;
import com.tsc.skuschenko.tm.api.service.IConnectionService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.repository.dto.UserDTORepository;
import com.tsc.skuschenko.tm.service.ConnectionService;
import com.tsc.skuschenko.tm.service.PropertyService;
import com.tsc.skuschenko.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import javax.persistence.EntityManager;

public class UserRepositoryTest {

    @NotNull
    final IPropertyService propertyService =
            new PropertyService();
    @NotNull
    final IConnectionService connectionService =
            new ConnectionService(propertyService);

    @NotNull
    final EntityManager entityManager =
            connectionService.getEntityManager();

    @Test
    public void testCreate() {
        @NotNull final UserDTO user = testUserModel();
        testRepository(user);
    }

    @Test
    public void testFindByEmail() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserDTORepository
                userRepository = testRepository(user);
        @Nullable final UserDTO userFind =
                userRepository.findByEmail(user.getEmail());
        entityManager.close();
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testFindByLogin() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserDTORepository
                userRepository = testRepository(user);
        @Nullable final UserDTO userFind =
                userRepository.findByLogin(user.getLogin());
        entityManager.close();
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testFindOneById() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserDTORepository
                userRepository = testRepository(user);
        @Nullable final UserDTO userFind =
                userRepository.findById(user.getId());
        entityManager.close();
        Assert.assertNotNull(userFind);
    }

    @Test
    public void testRemoveByLogin() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserDTORepository
                userRepository = testRepository(user);
        entityManager.getTransaction().begin();
        userRepository.removeByLogin(user.getLogin());
        entityManager.getTransaction().commit();
        @Nullable final UserDTO userFind =
                userRepository.findById(user.getId());
        entityManager.close();
        Assert.assertNull(userFind);
    }

    @Test
    public void testRemoveOneById() {
        @Nullable final UserDTO user = testUserModel();
        @NotNull final IUserDTORepository
                userRepository = testRepository(user);
        entityManager.getTransaction().begin();
        userRepository.removeOneById(user.getId());
        entityManager.getTransaction().commit();
        @Nullable final UserDTO userFind =
                userRepository.findById(user.getId());
        entityManager.close();
        Assert.assertNull(userFind);
    }

    @NotNull
    private IUserDTORepository testRepository(@NotNull final UserDTO user) {
        @NotNull final IUserDTORepository userRepository =
                new UserDTORepository(entityManager);
        entityManager.getTransaction().begin();
        userRepository.add(user);
        entityManager.getTransaction().commit();
        Assert.assertFalse(userRepository.findAll().isEmpty());
        @Nullable final UserDTO userById =
                userRepository.findById(user.getId());
        Assert.assertNotNull(userById);
        Assert.assertEquals(userById.getId(), user.getId());
        entityManager.close();
        return userRepository;
    }

    @NotNull
    private UserDTO testUserModel() {
        @Nullable final UserDTO user = new UserDTO();
        user.setFirstName("FirstName");
        user.setMiddleName("MiddleName");
        user.setEmail("Email@Email.ru");
        user.setLastName("LastName");
        user.setLogin("Login");
        user.setPasswordHash(HashUtil.salt("secret", 35484, "password"));
        Assert.assertNotNull(user);
        Assert.assertNotNull(user.getEmail());
        Assert.assertNotNull(user.getLogin());
        Assert.assertNotNull(user.getFirstName());
        Assert.assertNotNull(user.getLastName());
        Assert.assertNotNull(user.getMiddleName());
        Assert.assertNotNull(user.getPasswordHash());
        Assert.assertEquals("FirstName", user.getFirstName());
        Assert.assertEquals("MiddleName", user.getMiddleName());
        Assert.assertEquals("LastName", user.getLastName());
        Assert.assertEquals("Login", user.getLogin());
        Assert.assertEquals("Email@Email.ru", user.getEmail());
        Assert.assertEquals(
                HashUtil.salt("secret", 35484, "password"),
                user.getPasswordHash()
        );
        return user;
    }

}
